package io.bitbucket.javatek.excel;

/**
 *
 */
public class BadExcelException extends RuntimeException {
  BadExcelException(String message) {
    super(message);
  }

  BadExcelException(String message, Throwable cause) {
    super(message, cause);
  }
}
