package io.bitbucket.javatek.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 *
 */
public final class ExcelSheet implements Iterable<ExcelRow> {
  private final Sheet sheet;
  private final List<String> head;

  ExcelSheet(Sheet sheet) {
    this.sheet = requireNonNull(sheet);
    this.head = constructHead(sheet);
  }

  private List<String> constructHead(Sheet sheet) {
    Row firstRow = sheet.getRow(0);
    if (firstRow == null) {
      throw new BadExcelException(
        "Heading row not found on sheet " + sheet.getSheetName()
      );
    }

    List<String> head = new ArrayList<>();
    for (Cell cell : firstRow) {
      head.add(cell.toString());
    }
    return head;
  }

  @Nonnull
  @Override
  public Iterator<ExcelRow> iterator() {
    Iterator<Row> iterator = sheet.iterator();

    // пропускаем заголовок
    if (iterator.hasNext()) {
      iterator.next();
    }

    return new Iterator<ExcelRow>() {
      @Override
      public boolean hasNext() {
        return iterator.hasNext();
      }

      @Override
      public ExcelRow next() {
        return
          new ExcelRow(
            iterator.next(),
            ExcelSheet.this,
            head
          );
      }
    };
  }

  @Nonnull
  public ExcelRow addRow() {
    Row row = sheet.createRow(sheet.getLastRowNum() + 1);
    return new ExcelRow(row, this, head);
  }

  public void removeRow(@Nonnull ExcelRow row) {
    sheet.removeRow(sheet.getRow(row.rowNum()));
  }
}
