package io.bitbucket.javatek.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.LocaleUtil;
import org.bitbucket.javatek.email.Email;
import org.bitbucket.javatek.email.MalformedEmailException;
import io.bitbucket.javatek.phone.BadPhoneException;
import io.bitbucket.javatek.phone.Phone;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.apache.poi.util.LocaleUtil.getUserTimeZone;

/**
 *
 */
public final class ExcelRow {
  private final Row row;
  private final ExcelSheet sheet;
  private final List<String> head;

  ExcelRow(Row row, ExcelSheet sheet, List<String> head) {
    this.sheet = requireNonNull(sheet);
    this.row = requireNonNull(row);
    this.head = head;
  }

  public int rowNum() {
    return row.getRowNum();
  }

  @Nonnull
  public ExcelSheet sheet() {
    return sheet;
  }

  @Nonnull
  public String getString(String cellName) {
    Cell cell = getCell(cellName);
    switch (cell.getCellType()) {
      case STRING:
      case BLANK:
        return normalizeString(cell.getStringCellValue());
      case NUMERIC:
        if (DateUtil.isCellDateFormatted(cell, null)) {
          DateFormat sdf =
            new SimpleDateFormat(
              "dd-MMM-yyyy",
              LocaleUtil.getUserLocale()
            );
          sdf.setTimeZone(getUserTimeZone());
          return sdf.format(cell.getDateCellValue());
        }
        return
          BigDecimal
            .valueOf(cell.getNumericCellValue())
            .stripTrailingZeros()
            .toPlainString();
      case FORMULA:
        return cell.getCellFormula().trim();
      case BOOLEAN:
        return Boolean.toString(cell.getBooleanCellValue());
      default:
        throw new BadExcelException(
          MessageFormat.format(
            "Unexpected cell type [{1}, {2}]: {3}",
            1 + cell.getRowIndex(),
            1 + cell.getColumnIndex(),
            cell.getCellType()
          )
        );
    }
  }

  private String normalizeString(String string) {
    return string.replace(" ", "").trim();
  }

  public <T> ExcelRow setString(String cellName, @Nullable T value) {
    String string =
      value != null
        ? normalizeString(value.toString())
        : "";
    Cell cell = getCell(cellName);
    if (string.isEmpty()) {
      cell.setBlank();
    }
    else {
      cell.setCellValue(string);
    }
    return this;
  }

  @Nullable
  public LocalDate getDate(String cellName, DateTimeFormatter... formats) {
    if (formats.length == 0) {
      throw new IllegalArgumentException("No formats specified");
    }

    String string = getString(cellName);
    if (string.isEmpty()) {
      return null;
    }

    BadExcelException error =
      new BadExcelException(
        MessageFormat.format(
          "Cannot read date from {2}. See cell '{0}' in row {1}",
          cellName, rowNum(), string
        )
      );

    for (DateTimeFormatter format : formats) {
      try {
        return LocalDate.parse(string, format);
      }
      catch (DateTimeParseException e) {
        error.addSuppressed(e);
      }
    }

    throw error;
  }

  @Nullable
  public LocalDateTime getDateTime(String cellName, DateTimeFormatter... formats) {
    if (formats.length == 0) {
      throw new IllegalArgumentException("No formats specified");
    }

    String string = getString(cellName);
    if (string.isEmpty()) {
      return null;
    }

    BadExcelException error =
      new BadExcelException(
        MessageFormat.format(
          "Cannot read datetime from {2}. See cell '{0}' in row {1}",
          cellName, rowNum(), string
        )
      );

    for (DateTimeFormatter format : formats) {
      try {
        return LocalDateTime.parse(string, format);
      }
      catch (DateTimeParseException e) {
        error.addSuppressed(e);
      }
    }

    throw error;
  }

  @Nullable
  public Email getEmail(String cellName) {
    String string = getString(cellName);
    try {
      return
        !string.isEmpty()
          ? new Email(string)
          : null;
    }
    catch (MalformedEmailException e) {
      throw new BadExcelException(
        MessageFormat.format(
          "Cannot read email from {2}. See cell '{0}' in row {1}",
          cellName, rowNum(), string
        ),
        e
      );
    }
  }

  @Nullable
  public Phone getPhone(String cellName) {
    String string = getString(cellName);
    try {
      return
        !string.isEmpty()
          ? new Phone(string)
          : null;
    }
    catch (BadPhoneException e) {
      throw new BadExcelException(
        MessageFormat.format(
          "Cannot read phone num from {2}. See cell '{0}' in row {1}",
          cellName, rowNum(), string
        ),
        e
      );
    }
  }

  @Nonnull
  private Cell getCell(String cellName) {
    int cellNum = head.indexOf(cellName);
    if (cellNum < 0) {
      throw new IllegalArgumentException(
        MessageFormat.format(
          "There is no column named {0}",
          cellName
        )
      );
    }

    Cell cell = row.getCell(cellNum);
    return cell == null
      ? row.createCell(cellNum)
      : cell;
  }
}
