package io.bitbucket.javatek.excel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.NoSuchElementException;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.apache.poi.openxml4j.opc.PackageAccess.READ_WRITE;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 *
 */
public final class ExcelBook implements AutoCloseable {
  private final Workbook workbook;
  private final File file;

  public ExcelBook(File file) throws IOException {
    this.file = requireNonNull(file);
    try {
      this.workbook =
        new XSSFWorkbook(
          OPCPackage.open(file, READ_WRITE)
        );
    }
    catch (IOException | InvalidFormatException | RuntimeException e) {
      throw new IOException("Cannot open " + file, e);
    }
  }

  @Nonnull
  public ExcelSheet getSheet(String name) {
    Sheet sheet = workbook.getSheet(name);
    if (sheet == null) {
      throw new NoSuchElementException(
        MessageFormat.format("Sheet {0} not found", name)
      );
    }
    return new ExcelSheet(sheet);
  }

  public void save() throws IOException {
    File temp = File.createTempFile("javatek-excel", ".tmp");
    try {
      try (OutputStream os = new FileOutputStream(temp)) {
        workbook.write(os);
      }
      catch (IOException e) {
        throw new IOException("Cannot write to " + file, e);
      }
      Files.move(
        temp.toPath(),
        file.toPath(),
        REPLACE_EXISTING
      );
    }
    catch (IOException e) {
      try {
        Files.delete(temp.toPath());
      }
      catch (IOException e2) {
        e.addSuppressed(e2);
      }
      throw e;
    }
  }

  @Override
  public void close() {
    try {
      workbook.close();
    }
    catch (IOException | RuntimeException ignored) {}
  }
}
